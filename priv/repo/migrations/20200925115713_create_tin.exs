defmodule Euler.Repo.Migrations.CreateTin do
  use Ecto.Migration

  def change do
    create table(:tin) do
      add :date, :naive_datetime
      add :number, :string
      add :is_correct, :boolean, default: false, null: false

      timestamps()
    end

  end
end
