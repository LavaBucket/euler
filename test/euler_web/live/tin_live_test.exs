defmodule EulerWeb.TinLiveTest do
  use EulerWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Euler.Log

  @create_attrs %{date: ~N[2010-04-17 14:00:00], is_correct: true, number: "some number"}
  @update_attrs %{date: ~N[2011-05-18 15:01:01], is_correct: false, number: "some updated number"}
  @invalid_attrs %{date: nil, is_correct: nil, number: nil}

  defp fixture(:tin) do
    {:ok, tin} = Log.create_tin(@create_attrs)
    tin
  end

  defp create_tin(_) do
    tin = fixture(:tin)
    %{tin: tin}
  end

  describe "Index" do
    setup [:create_tin]

    test "lists all tin", %{conn: conn, tin: tin} do
      {:ok, _index_live, html} = live(conn, Routes.tin_index_path(conn, :index))

      assert html =~ "Listing Tin"
      assert html =~ tin.number
    end

    test "saves new tin", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.tin_index_path(conn, :index))

      assert index_live |> element("a", "New Tin") |> render_click() =~
               "New Tin"

      assert_patch(index_live, Routes.tin_index_path(conn, :new))

      assert index_live
             |> form("#tin-form", tin: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#tin-form", tin: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.tin_index_path(conn, :index))

      assert html =~ "Tin created successfully"
      assert html =~ "some number"
    end

    test "updates tin in listing", %{conn: conn, tin: tin} do
      {:ok, index_live, _html} = live(conn, Routes.tin_index_path(conn, :index))

      assert index_live |> element("#tin-#{tin.id} a", "Edit") |> render_click() =~
               "Edit Tin"

      assert_patch(index_live, Routes.tin_index_path(conn, :edit, tin))

      assert index_live
             |> form("#tin-form", tin: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#tin-form", tin: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.tin_index_path(conn, :index))

      assert html =~ "Tin updated successfully"
      assert html =~ "some updated number"
    end

    test "deletes tin in listing", %{conn: conn, tin: tin} do
      {:ok, index_live, _html} = live(conn, Routes.tin_index_path(conn, :index))

      assert index_live |> element("#tin-#{tin.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#tin-#{tin.id}")
    end
  end

  describe "Show" do
    setup [:create_tin]

    test "displays tin", %{conn: conn, tin: tin} do
      {:ok, _show_live, html} = live(conn, Routes.tin_show_path(conn, :show, tin))

      assert html =~ "Show Tin"
      assert html =~ tin.number
    end

    test "updates tin within modal", %{conn: conn, tin: tin} do
      {:ok, show_live, _html} = live(conn, Routes.tin_show_path(conn, :show, tin))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Tin"

      assert_patch(show_live, Routes.tin_show_path(conn, :edit, tin))

      assert show_live
             |> form("#tin-form", tin: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#tin-form", tin: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.tin_show_path(conn, :show, tin))

      assert html =~ "Tin updated successfully"
      assert html =~ "some updated number"
    end
  end
end
