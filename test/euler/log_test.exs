defmodule Euler.LogTest do
  use Euler.DataCase

  alias Euler.Log

  describe "tin" do
    alias Euler.Log.Tin

    @valid_attrs %{date: ~N[2010-04-17 14:00:00], is_correct: true, number: "some number"}
    @update_attrs %{date: ~N[2011-05-18 15:01:01], is_correct: false, number: "some updated number"}
    @invalid_attrs %{date: nil, is_correct: nil, number: nil}

    def tin_fixture(attrs \\ %{}) do
      {:ok, tin} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Log.create_tin()

      tin
    end

    test "list_tin/0 returns all tin" do
      tin = tin_fixture()
      assert Log.list_tin() == [tin]
    end

    test "get_tin!/1 returns the tin with given id" do
      tin = tin_fixture()
      assert Log.get_tin!(tin.id) == tin
    end

    test "create_tin/1 with valid data creates a tin" do
      assert {:ok, %Tin{} = tin} = Log.create_tin(@valid_attrs)
      assert tin.date == ~N[2010-04-17 14:00:00]
      assert tin.is_correct == true
      assert tin.number == "some number"
    end

    test "create_tin/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Log.create_tin(@invalid_attrs)
    end

    test "update_tin/2 with valid data updates the tin" do
      tin = tin_fixture()
      assert {:ok, %Tin{} = tin} = Log.update_tin(tin, @update_attrs)
      assert tin.date == ~N[2011-05-18 15:01:01]
      assert tin.is_correct == false
      assert tin.number == "some updated number"
    end

    test "update_tin/2 with invalid data returns error changeset" do
      tin = tin_fixture()
      assert {:error, %Ecto.Changeset{}} = Log.update_tin(tin, @invalid_attrs)
      assert tin == Log.get_tin!(tin.id)
    end

    test "delete_tin/1 deletes the tin" do
      tin = tin_fixture()
      assert {:ok, %Tin{}} = Log.delete_tin(tin)
      assert_raise Ecto.NoResultsError, fn -> Log.get_tin!(tin.id) end
    end

    test "change_tin/1 returns a tin changeset" do
      tin = tin_fixture()
      assert %Ecto.Changeset{} = Log.change_tin(tin)
    end
  end
end
