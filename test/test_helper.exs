ExUnit.configure formatters: [ExUnit.CLIFormatter, ExUnitNotifier]
ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(Euler.Repo, :manual)
# cheatsheet:
# --only
#   describe
#   var:value
#   test "tag1 tag2 tag3"
# --exclude
# --include
# --stale
# --listen-on-stdin
# --failed
# --max-failures 1
# --seed 0
# {:mix_test_watch, "~> 0.8", only: :dev, runtime: false}
# mix test.watch
# mix test.watch --stale --max-failures 1 --trace --seed 0
