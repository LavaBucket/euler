defmodule EulerWeb.TinLive.Show do
  use EulerWeb, :live_view

  alias Euler.Log

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:tin, Log.get_tin!(id))}
  end

  defp page_title(:show), do: "Show Tin"
  defp page_title(:edit), do: "Edit Tin"
end
