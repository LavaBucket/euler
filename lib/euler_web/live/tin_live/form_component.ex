defmodule EulerWeb.TinLive.FormComponent do
  use EulerWeb, :live_component

  alias Euler.Log

  @impl true
  def update(%{tin: tin} = assigns, socket) do
    changeset = Log.change_tin(tin)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"tin" => tin_params}, socket) do
    changeset =
      socket.assigns.tin
      |> Log.change_tin(tin_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"tin" => tin_params}, socket) do
    save_tin(socket, socket.assigns.action, tin_params)
  end

  defp save_tin(socket, :edit, tin_params) do
    case Log.update_tin(socket.assigns.tin, tin_params) do
      {:ok, _tin} ->
        {:noreply,
         socket
         |> put_flash(:info, "Tin updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_tin(socket, :new, tin_params) do
    case Log.create_tin(tin_params) do
      {:ok, _tin} ->
        {:noreply,
         socket
         |> put_flash(:info, "Tin created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
