defmodule EulerWeb.TinLive.Index do
  use EulerWeb, :live_view

  alias Euler.Log
  alias Euler.Log.Tin

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :tin_collection, list_tin())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Tin")
    |> assign(:tin, Log.get_tin!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Tin")
    |> assign(:tin, %Tin{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Tin")
    |> assign(:tin, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    tin = Log.get_tin!(id)
    {:ok, _} = Log.delete_tin(tin)

    {:noreply, assign(socket, :tin_collection, list_tin())}
  end

  defp list_tin do
    Log.list_tin()
  end
end
