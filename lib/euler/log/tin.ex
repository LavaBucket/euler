defmodule Euler.Log.Tin do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tin" do
    field :date, :naive_datetime
    field :is_correct, :boolean, default: false
    field :number, :string

    timestamps()
  end

  @doc false
  def changeset(tin, attrs) do
    tin
    |> cast(attrs, [:date, :number, :is_correct])
    |> validate_required([:date, :number, :is_correct])
  end
end
